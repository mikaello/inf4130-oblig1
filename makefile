JFLAGS = -g
JC = javac
JVM= java
TESTFILE= oblig1-oppg2-test.txt test.log
FILE=
.SUFFIXES: .java .class
.java.class:
	$(JC) $(JFLAGS) $*.java

CLASSES = MatrixPrinter.java \
	FileReader.java \
	ObligUtils.java \
	Exercise1B.java \
	Exercise1D.java \
	Exercise2.java

MAIN = Exercise2

default: classes

classes: $(CLASSES:.java=.class)

run: classes
	$(JVM) $(MAIN) $(TESTFILE)

clean:
	$(RM) *.class *~ *.dvi *.log *.aux *.bbl *.blg *.ilg *.toc *.lof *.lot *.idx *.ind *.out *.ps
