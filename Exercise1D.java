import java.util.LinkedList;

/**
 * A class that solves the problem with the sum of selection (sos) by
 * topDown
 *
 * @author mikaello
 */
public class Exercise1D {
    public static void main(String[] args) {
	ObligUtils.runProgram(args[0], args[1], ObligUtils.topDown);
    }

    /**
     * Recursive memoized top down method to fill a boolean table with
     * true values in spots that is in the selection. This is the
     * resulting boolean double array for a given instance (with
     * answer):
     *
     * INSTANCE 3 8 1 5 3
     * YES (1,0)(5,1)(3,1)
     *
     *       0  1  2  3  4  5  6  7  8  9
     * 0( 1) -  -  -  -  -  -  -  -  -  -
     * 1( 5) -  -  -  -  -  T  -  -  -  -
     * 2( 3) -  -  -  -  -  -  -  -  T  -
     *
     * @param u boolean double array to store solutions
     * @param n numbers to select from
     * @param i row index in U and index in N, beginning at N.length-1
     * @param k column in in U and K constant (only at invocation)
     */
    public static boolean recursiveMemoizedTopDown(boolean[][] u, int[] n,
						   int i, int k) {
	if (i < 0) {
	    // No numbers here
	    return false;
	}

	// Memoization
	if (u[i][k]) {
	    return true;
	}

	// Difference between K and current number in N
	int diff = k-n[i];

	if (diff == 0) {
	    // MATCH - last number in selection
	    return u[i][k] = true;
	}


	if (diff < 0) {
	    // This was not in the selection, ignore
	    return recursiveMemoizedTopDown(u, n, i-1, k);
	}

	// Add this or test for next (ignore this)
	return u[i][k] = recursiveMemoizedTopDown(u, n, i-1, diff) ||
	    (recursiveMemoizedTopDown(u, n, i-1, k) && i != n.length-1);
    }

    /**
     * Finds the selection from a boolean double array. It uses a
     * single for-loop, this is because a number in N can only occur
     * once. We start from the bottom, at N.length-1, until we find a
     * true value, then we now that this row index in N is in the
     * selection. Then we can subtract this N value from the column
     * value to get the next number.
     *
     * @see recursiveMemoizedTopDown
     * @param u boolean double array that contains the answer to be
     * extracted
     * @param n numbers to select from
     * @param k k value, sum of the selection to be found
     * @return the selection that matches k
     */
    public static LinkedList<Integer> getSolutionFromTopDown(boolean[][] u, int[] n,int k) {
	LinkedList<Integer> selection = new LinkedList<Integer>();

	// Traverses from last element to first
	for (int i = u.length-1; i >= 0; i--) {
	    if (k > 0 && u[i][k]) {

		if (!(i != 0 && u[i-1][k])) {
		    selection.add(n[i]);

		    // "Removes" this number, and try to find a selection
		    // that match the rest
		    k -= n[i];
		}
	    }
	}

	return selection;
    }
}
