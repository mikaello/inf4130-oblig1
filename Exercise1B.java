import java.util.LinkedList;

/**
 * A class that solves the problem with the sum of selection (sos) by
 * topDown
 *
 * @author mikaello
 */
public class Exercise1B {
    public static void main(String[] args) {
	ObligUtils.runProgram(args[0], args[1], ObligUtils.bottomUp);
    }

    /**
     * This dynamic bottom up algorithm fills a boolean double array
     * in the following manner:
     *
     *       0  1  2  3  4  5  6  7  8  9
     * 0( 1) T  T  -  -  -  -  -  -  -  -
     * 1( 5) T  T  -  -  -  T  T  -  -  -
     * 2( 3) T  T  -  T  T  T  T  -  T  T
     *
     */
    public static void dynamicBottomUp(boolean[][] u, int[] n) {
	for (int i = 0; i < u.length; i++) {
	    for (int j = 1; j < u[i].length; j++) {
		u[i][j] = j == n[i] || (i != 0 && u[i-1][j]);

		if (j >= n[i] && !u[i][j]) {
		    u[i][j] = (i != 0) && u[i-1][j-n[i]];
		}
	    }
	}
    }

    /**
     * ...
     *
     * @param k K, the starting point, goal
     * @return a linked list with numbers that contains the selection
     * that sums to K
     */
    public static LinkedList<Integer> getSolutionFromBottomUp(boolean[][] u, int[] n, int k) {
	// Selection that sums to K
	LinkedList<Integer> selection = new LinkedList<Integer>();

	// Starting value
	int j = k;

	for (int i = u.length-1; i >= 0; i--) {
	    if (u[i][j]) {
		 if (j - n[i] == 0) {
		     selection.add(n[i]);
		     break;
		 }

		 if (i <= 0) {
		     System.out.println("YOUR INPUT IS WRONG");
		     break;
		 }

		 if (!u[i-1][j]) {
		     selection.add(n[i]);
		     j -= n[i];
		 }
	    }
	}

	return selection;
    }


}
