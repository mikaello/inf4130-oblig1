import java.util.LinkedList;
import java.util.Arrays;
import java.io.PrintWriter;
import java.io.File;
import java.io.FileNotFoundException;

public class ObligUtils {
    public static final boolean topDown = true;
    public static final boolean bottomUp = !topDown;

    /**
     * Run program
     * @param filename which file to parse
     * @param useTopDown which method to calculate with
     */
    public static void runProgram(String inFilename, String outFilename, boolean useTopDown) {
	LinkedList<int[]> nums = ObligUtils.getVariablesFromFile(inFilename);

	String answers = ObligUtils.solveInput(-1, nums, useTopDown);
	printToFile(outFilename, answers);
    }


    /**
     * This return a linked list of integer vectors from a specified
     * file. Each vector is on the following form:
     *
     * N K x x x x x
     *
     * Where N denotes how many x that exists, and K is a constant.
     *
     * @param filename file to read numbers from
     * @return a linked list with all the lines in the file, where
     * every second element is the beginning of a new line
     */
    public static LinkedList<int[]> getVariablesFromFile(String filename) {
	return FileReader.readNumbers(FileReader.openFile(filename));
    }

    /**
     * Solves a given number of solutions, be getting a new line,
     * extracting N, calculating S and calling getSolution() with
     * K. Finally it prints the answers by calling
     * MatrixPrinter.printAnswer()
     *
     * @see Oblig1.getSolution
     * @see MatrixPrinter.printAnswer
     * @param numberToSolve number of solutions, if negative, read all
     * @param inputLines linked list int vectors, where each vector is
     * a new line
     * @param topdown will solve SOS top-down when true
     * @param all the answers (YES or NO with selection)
     */
    public static String solveInput(int numberToSolve,
				  LinkedList<int[]> inputLines,
				  boolean topdown) {
	// If number is negative, then read all lines
        if (numberToSolve < 0)
	    numberToSolve = inputLines.size();

	LinkedList<Integer> solution;

	StringBuilder sb = new StringBuilder();

	while (numberToSolve != 0 && !inputLines.isEmpty()) {
	    int[] instance = inputLines.pollFirst();

	    // Get new N
	    int[] n = Arrays.copyOfRange(instance, 2, instance.length);

	    // Get new K
	    int k = instance[1];

	    // Calculate new S
	    int s = sumArray(n);

	    // Calcule new U (double boolean array)
	    boolean[][] u = new boolean[n.length][s+1];

	    if (topdown) {
		// Make u filled and get solution
		Exercise1D.recursiveMemoizedTopDown(u, n, n.length-1, k);
		solution = Exercise1D.getSolutionFromTopDown(u, n, k);
	    } else {
		// Make u filled and get solution
		Exercise1B.dynamicBottomUp(u,n);
		solution = Exercise1B.getSolutionFromBottomUp(u, n, k);
	    }

	    // Print only answer
	    sb.append(MatrixPrinter.printAnswer(instance, solution));


	    //Printing whole matrix (uncomment for visual matrix):
	    //MatrixPrinter.printBooleanMatrix(u, n);

	    numberToSolve--;

	}

	return sb.toString();

    }

    /**
     * Sums up all the values in input and returns the sum.  This is
     * the same as the value S
     * @param input vector of integers
     * @return sum of all the integers in the vector
     */
    public static int sumArray(int[] input) {
	int sum = 0;
	for (int e : input)
	    sum += e;

	return sum;
    }

    /**
     * Clears a boolean double array (make all fields false).
     * @param b boolean double array to be cleared
     */
    public static void clearBooleanDoubleArray(boolean[][] b) {
	for (int i = 0; i < b.length; i++) {
	    for (int j = 0; j < b[i].length; j++) {
		b[i][j] = false;
	    }
	}
    }

    public static void printToFile(String filename, String text) {
	PrintWriter out = null;

	try {
	    out = new PrintWriter(new File(filename));
	    out.print(text);
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	    System.exit(1);
	} finally {
	    out.close();
	}
    }

}
