import java.util.LinkedList;

/**
 * Helper class for printing matrices and vectors.
 */
public class MatrixPrinter {
    /**
     * Prints a boolean matrix, with symbols "T" representing true,
     * and "-" representing false. Indices will be printed on the x
     * and y-axis. With the corresponding "n" numbers in parantheses
     * on the y-axis.
     *
     * "i" is index on x-axis, and "j" is index on y-axis, "n" is the
     * corresponding number in the vector "n"
     *
     *      i i i
     * j(n) T - -
     * j(n) T - T
     *
     * @param matrix boolean matrix to be printed
     * @prama n integer vector that has values that corresponds to the
     * rows in the matrix
     */
    public static void printBooleanMatrix(boolean[][] matrix, int[] n) {
        for (int i = -1; i < matrix.length; i++) {
	    // Prints information for y-axis:
	    if (i == n.length) {
		// Last row
		System.out.printf("%d     ", i);
	    } else if (i >= 0) {
		// Middle rows
		System.out.printf("%d(%2d) ", i, n[i]);
	    } else {
		// First row
		System.out.print("      ");
	    }

            for (int j = 0; j < matrix[0].length; j++) {
                if (i == -1) {
		    // Prints information for x-axis:
		    if (j < 9)
			System.out.print(j + "  ");
		    else
			System.out.print(j + " ");
                } else {
		    // Prints boolean content of matrix:
                    if (matrix[i][j])
                        System.out.print("T  ");
                    else {
                        System.out.print("-  ");
                    }
                }
            }
            System.out.println();
        }
    }


    /**
     * Prints out a vector with the corresponding indices in the top
     * row. If "i" is index, and "e" is element, it will be like this:
     *
     * i i i i i
     * e e e e e
     *
     * @param vector the vector to be printed
     */
    public static void printIntegerVector(int[] vector) {
	System.out.print("Index: ");
	for (int i = 0; i < vector.length; i++) {
	    System.out.printf("%2d ", i);
	}

	System.out.print("\nValue: ");
	for (int i = 0; i < vector.length; i++) {
	    System.out.printf("%2d ", vector[i]);
	}

	System.out.println("\n");

    }

    /**
     * Prints out instance and selection if it exists. It follows the following format:
     *
     * INSTANCE 5 14 1 5 6 3 10
     * YES (1,1)(5,0)(6,0)(3,1)(10,1)
     *
     * INSTANCE 5 4 2 5 6 3 10
     * NO
     *
     * @param instance input instance
     * @param selection selection of numbers if it exists
     */
    public static String printAnswer(int[] instance, LinkedList<Integer> selection) {
	StringBuilder sb = new StringBuilder("INSTANCE ");

	for (int e : instance) {
	    sb.append(e + " ");
	}

	sb.append("\n");

	if (selection == null || selection.isEmpty()) {
	    sb.append("NO\n\n");
	} else {
	    sb.append("YES ");
	    for (int i = 2; i < instance.length; i++) {
		sb.append(String.format("(%d,%d)", instance[i],
					selection.remove(new Integer(instance[i])) ? 1 : 0));
	    }
	    sb.append("\n\n");
	}

	return sb.toString();
    }
}
