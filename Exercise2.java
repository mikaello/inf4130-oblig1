import java.util.LinkedList;

/**
 * @author mikaello
 */
public class Exercise2 {
    public static void main(String[] args) {
        Trie t = new Trie(args[0]);

        t.insert();
        t.search();

	// t.print();

	ObligUtils.printToFile(args[1], t.toString());
	System.out.println("Output written to file: " + args[1]);
    }
}

class Trie {
    /* Letters in the alphabet = max number of leafs */
    public static final int lettersInTheEnglishAlphabet = 26;
    /* Nodes in the trie */
    private String[] nodes;
    /* Words to be searched for */
    private LinkedList<String> words;
    /* Contains output for insertions */
    private StringBuilder sb;
    /* Head of the nodes */
    private Node head;


    Trie(String inFilename) {
        words = new LinkedList<String>();
        sb = new StringBuilder();
        head = new Node('\u0000');

        // Reads nodes and search words from outFilename
        nodes = FileReader.readWords(FileReader.openFile(inFilename), words);
    }

    /**
     * Inserts all the words into the trie, and makes output.
     */
    public void insert() {
        StringBuilder output;
        for (String word : nodes) {
            sb.append(word + " ");
            output = new StringBuilder();
            if (head.insert(word.toCharArray(), 0, output, false)) {
                output.append("(");
                head.traverseTree(output);
                output.append(")");
            }

            sb.append(output);
            sb.append("\n");
        }
    }

    /**
     * Calls the find method in the trie, with all the words.
     */
    public void search() {
        for (String w : words) {
            sb.append(w);
            sb.append(head.find(w.toCharArray(), 0));
        }
    }

    public void print() {
        System.out.println(sb.toString());
    }

    /**
     * Nodeclass of the trie
     */
    private class Node {
        Node[] leafnodes = new Node[lettersInTheEnglishAlphabet];
        char info;
	int numBranches = 0;

        /* Helper variabel, so you don't have to traverse leafnode see
           if a branch exists: */
        boolean hasBranch = false;

        Node(char info) {
            this.info = info;
        }

	/**
	 * Traverses tree and gives the same output as in the
	 * assignment specification.
	 */
        public void traverseTree(StringBuilder s) {
            if (info != '\u0000')
                s.append(info);

            for (Node n : leafnodes) {
                if (n != null) {
                    if (moreThanOneLeaf())
                        s.append('(');
                    n.traverseTree(s);
                    if (moreThanOneLeaf())
                        s.append(')');
                }

            }
        }

	/**
	 * @return true if there is more than one child of this node
	 */
        public boolean moreThanOneLeaf() {
	    return numBranches >= 2;
        }

	/**
	 * Inserts a word to a trie. This is done semi-recursively, so
	 * that every node just inserts one letter and pass on the
	 * task to that letter with the remaining letters.
	 *
	 * @param word word to be inserted
	 * @param index index in word
	 * @param output will contain PREFIX or EXTENSION if that
	 * happens, else nothing
	 * @param newlyCreated true if this node creates a new node
	 * @return true if the word was inserted, false in the case of
	 * PREFIX or EXTENSION
	 */
        public boolean insert(char[] word, int index, StringBuilder output,
			      boolean newlyCreated) {
            // Length of word has been reached
            if (index == word.length) {
                if (hasBranch) {
                    // The word is still going, but we are done: this is a prefix
                    output.append("PREFIX");
                    return false;
                } else {
                    // A new word in the trie has been created
                    return true;
                }
            }


            if (leafnodes[word[index]-'a'] == null) {
                // This letter does not exists, create a new
                if (info == '\u0000' || hasBranch || newlyCreated) {
                    /*
                      If it has a branch, it means we are now
                      diverging. If it is newly created we are still
                      creating a word
                    */
                    hasBranch = true;
                    leafnodes[word[index]-'a'] = new Node(word[index]);
		    numBranches++;
                    newlyCreated = true;
                } else {
                    output.append("EXTENSION");
                    return false;
                }
            }

            boolean b = leafnodes[word[index]-'a'].insert(word,++index, output, newlyCreated);

            return b;
        }

	/**
	 * A "recursive" search for a given word, letter by letter.
	 *
	 * @param word a char array for the word to search for
	 * @param index index in the given word
	 * @return YES if the given word was found, NO else
	 */
        public String find(char[] word, int index) {
	    // Base case, word found!
            if (word.length == index) {
                return " YES\n";
            }

            if (leafnodes[word[index]-'a'] != null) {
		// Recursive search for a word
                return leafnodes[word[index]-'a'].find(word,++index);
            }

            return " NO\n";
        }

        public String toString() {
            return " " + info + " ";
        }
    }

    public String toString() {
	return sb.toString();
    }
}
