import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;

public class FileReader {
    /**
     * Creates a Scanner object from a file with a name given as
     * parameter
     * @param filename filename to file that should be opened
     * @return Scanner object of the specified file
     */
    public static Scanner openFile(String filename) {
	try {
	    return new Scanner(new File(filename));
	} catch (FileNotFoundException e) {
	    System.err.println("An error occured when trying to open the file "
			       + new File(filename).getAbsolutePath());
	    e.printStackTrace();
	}
	return null;
    }

    /**
     * Reads number from a file in the following format:
     * x y z z z z z z
     * x y z z z z
     * x y z z z z z z z z z
     *
     * Where x, y and z is numbers. x denotes how many z it exists (it
     * could by arbitrarely many). y is the number K.
     *
     * @param scan Scanner object which contains a file with numbers
     * in the given format
     * @return a linked list of int arrays, where each int array is a
     * row in the file
     */
    public static LinkedList<int[]> readNumbers(Scanner scan) {
	LinkedList<int[]> lists = new LinkedList<int[]>();

	while (scan.hasNextInt()) {
	    int n = scan.nextInt();
	    int k = scan.nextInt();

	    int[] input = new int[n+2];
	    input[0] = n;
	    input[1] = k;

	    for (int i = 2; i < input.length; i++) {
		input[i] = scan.nextInt();
	    }

	    lists.add(input);
	}

	return lists;
    }

    /**
     * Reads a file in the format:
     *
     * n
     * x
     * x
     * x
     * y
     * y
     * y
     * y
     *
     * Where n is a number that denotes how many strings x that
     * follows, after that follows a arbitrary number of strings y.
     *
     * @param scan Scanner object which contains a file with words
     * in the given format
     * @param words this list will be filled with words to search for
     * @return an array with the node-words
     */
    public static String[] readWords(Scanner scan, LinkedList<String> words) {
	int n = scan.nextInt();

	String[] nodes = new String[n];

	for (int i = 0; i < n; i++) {
	    nodes[i] = scan.next();
	}

	while(scan.hasNext()) {
	    words.add(scan.next().trim());
	}

	return nodes;
    }
}
